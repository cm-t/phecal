# Contributing

When contributing to this repository please first discuss the change you wish to make via [issue](https://gitlab.com/cm-t/phecal/-/issues/).

Note we have a [code of conduct](https://gitlab.com/cm-t/phecal/-/blob/master/CODE_OF_CONDUCT.md): please follow it in all your interactions with the project.

## Pull Request Process

0. Commits should respect the [conventionnal commit](https://www.conventionalcommits.org/) norm
1. Ensure dependecies are updated. Run `composer update`
2. You may request the main reviewer to merge the Pull Reuest for you
