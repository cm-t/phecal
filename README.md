
# 🐘PHP & 📒Calepin = 💩Phécal

![Phecal logo](https://gitlab.com/cm-t/phecal/-/raw/master/docs/Phecal.png) 
💩 Phécal, your static site generator to add big material.


[![License](https://poser.pugx.org/Cecil/Cecil/license)](https://gitlab.com/cm-t/phecal/-/blob/master/LICENSE)  


Phécal is a CLI application that merges plain text files (written in [Markdown](http://daringfireball.net/projects/markdown/)), images and [Twig](https://twig.symfony.com) into a folder with html.

📄 [Documentation](https://Cecil.app/documentation) | 💻 [Demo](https://demo.Phécal.app) | 🐛 [Issue tracker](https://gitlab.com/cm-t/phecal/-/issues) | Read the [Quick Start](https://Cecil.app/documentation/quick-start/) documentation page.


## Features

- No database, no server, no dependency: performance and security
- Your pages are stored in [Markdown](https://daringfireball.net/projects/markdown/) flat files with a [YAML front matter](https://Cecil.app/documentation/content/#front-matter)
- Powered by [Twig](https://twig.symfony.com/doc/templates.html), a flexible template engine, with themes support
- Pagination, sitemap, redirections, robots.txt, taxonomies, RSS are generated automatically
- Handles and optimizes ahttps://gitlab.com/cm-t/phecal/-/blob/master/docs/Phecal.png

Then install the binary globally:

```bash
mv phecal.phar /usr/local/bin/phecal
chmod +x /usr/local/bin/phecal
```

> [PHP](https://www.php.net) 7.4+ is required.

## Usage

- Get help: `phecal help`
- Create new website: `phecal new:site`
- Build and serve it: `phecal serve`

## Contributing

See [Contributing](CONTRIBUTING.md).

## Fork

This project is a fork of the great [Cecil](https://cecil.app/) project. Currently the documentation is linked to their website.  
This fork is an important step forward to provide more `matière` to your web calepin *(notebook, blog)*, using `phecal`.  
Very serious. Have fun :)

### Features

This fork integrates a great pun in its naming, but it has also some goals for next implementation:
 - finish renaming namespaces and setup the CI at gitlab
 - add a new build package, a [snap](https://snapcraft.io/) to allow automatised update and integration on every systems that runs `snapd` and also for those using ubuntu core as a server.
 - make l10n easier on default theme
 - RGPD notice
 - eventually add a basic search module *(since we do static sites, will probably rely on a js parsing the rss for eg)*
 - Nextcloud integration is something that would be cool *(coming from a picoCMS experience with failed support in nextcloud updates, i hope we can have something good here; and since content will be static after being generated, we shall strongly limit introducing security issues to nextcloud servers)*. Users should be able to 
   - choose a folder for their website content (md, theme, assets, …)
   - choose the url to expose website via nextcloud's url
   - generate the content, and generaly provide a (web) GUI to expose phecal's build-in commands
   - it will be in another repos, but will include phecal's build
 - …


## License

Phécal is a free software distributed under the terms of the MIT license.

Phécal © Phécal contributors  
Phécal is based on [Cecil](https://cecil.app/), which is created by [Arnaud Ligny](mailto:arnaud@ligny.fr) and maintained by Cecil contributors  
Phécal Logo © CC-by-sa cm-t