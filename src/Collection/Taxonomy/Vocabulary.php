<?php

declare(strict_types=1);

/*
 * This file is part of Cecil.
 *
 * Copyright (c) Arnaud Ligny <arnaud@ligny.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Phecal\Collection\Taxonomy;

use Phecal\Collection\Collection as CecilCollection;
use Phecal\Collection\CollectionInterface;
use Phecal\Collection\ItemInterface;

/**
 * Class Vocabulary.
 */
class Vocabulary extends PhecalCollection implements ItemInterface
{
    /**
     * Adds a term to a Vocabulary collection.
     * {@inheritdoc}
     */
    public function add(ItemInterface $item): CollectionInterface
    {
        if ($this->has($item->getId())) {
            // return if already exists
            return $this;
        }

        $this->items[] = $item;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $id): Term
    {
        return parent::get($id);
    }
}
